# If empty, use the chart name as common base name for any FROST-Server Kubernetes component
name:
clusterDomain: cluster.local

frost:
  ######################################
  # FROST-Server general configuration #
  ######################################
  enableActuation: false
  securityContext:
    fsGroup: 1000

  containerSecurityContext:
    runAsUser: 1000
    runAsNonRoot: true

  db:
    # Common FROST-Server Database properties, shared by any other FROST-Server modules
    implementationClass: "de.fraunhofer.iosb.ilt.sta.persistence.postgres.longid.PostgresPersistenceManagerLong"
    idGenerationMode: "ServerGeneratedOnly"
    persistenceEnabled: true
  ################################################
  # FROST-Server HTTP module configuration GLOBAL#
  ################################################
  httpGlobal:
    image:
      registry: docker.io
      repository: fraunhoferiosb/frost-server-http
      tag: 2.1.0
      pullPolicy: IfNotPresent

  ##################################################
  # FROST-Server HTTP module configuration INTERNAL#
  ##################################################
  httpInternal:
    enabled: false
    # FROST-Server HTTP deployment settings
    urlSubPath:
    defaultCount: false
    defaultTop: 100
    maxTop: 1000
    maxDataSize: 25000000
    useAbsoluteNavigationLinks: true
    enableAuthentification: false
    mqttExposedEndpoints:
    persistence:
      countMode: "FULL" # SMAPLE_LIMIT | LIMIT_SAMPLE | LIMIT_ESTIMATE | ESTIMATE_LIMIT
      countEstimateThreshold: 10000
    extensions:
      customLinks:
        enable: true
        recursiveDepth: 0 # default: 0
      filterDelete:
        enable: false
    plugins:
      odata:
        enable: false
      openApi:
        enable: true
    #
    # k8s related settings
    resources:
      requests:
        cpu: 300m
        memory: 900Mi
      limits:
        cpu: 300m
        memory: 900Mi

    # Liveness probe configuration
    livenessProbe:
      enabled: false
      httpGet:
        path: /FROST-Server/v1.1/
        port: tomcat
      initialDelaySeconds: 30
      periodSeconds: 10
      timeoutSeconds: 1
      successThreshold: 1
      failureThreshold: 2

    readinessProbe:
      enabled: false
      httpGet:
        path: '/FROST-Server/v1.1/Things(6632)'
        port: tomcat
      initialDelaySeconds: 60
      periodSeconds: 10
      timeoutSeconds: 2
      successThreshold: 1
      failureThreshold: 2
    # Horizontal Pod Autoscaler
    hpa:
      enabled: true
      min: 1
      max: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    #-- either you set hpa.enabled: true or you use replicas
    replicas: 1
    ports:
      http:
        nodePort:
        servicePort: 80

    # FROST-Server HTTP business settings
    serviceHost: fqdn-frost.changeme.de
    serviceProtocol: https
    servicePort:
        # if ingress is false, define a nodePort above
    ingress:
      enabled: true

    # FROST-Server Authentication related settings
    auth:
      provider: de.fraunhofer.iosb.ilt.frostserver.auth.keycloak.KeycloakAuthProvider
      allowAnonymousRead: true
      roleRead: read
      roleCreate: create
      roleUpdate: update
      roleDelete: delete
      roleAdmin: admin
      keycloakConfigUrl: https://fqdn-keycloak.changeme.de/auth/realms/frostrealm/clients-registrations/install/frostserver
      keycloackConfigSecret: changeme-secret-from-frostserver-client-in-keycloak

    # FROST-Server Database related settings to the FROST-Server HTTP
    db:
      autoUpdate: true
      alwaysOrderbyId: false
      maximumConnection: 10
      maximumIdleConnection: 10
      minimumIdleConnection: 10
      queryTimeout: 0
      slowQueryThreshold: 200

      # if ip left blank, it'll take the name of the service
      ip: changeme-postgrsql-ip
      ports:
        postgresql:
          servicePort: 5432

      ## configuration for frost-server ##
      frostConnection:
        database: changeme-sensorthingsdb
        username: changeme-sensorthingsusr
        password: changeme

    # FROST-Server Messages Bus related settings to the FROST-Server HTTP
    bus:
      sendWorkerPoolSize: 100
      sendQueueSize: 1000
      recvWorkerPoolSize: 10
      recvQueueSize: 100
      maxInFlight: 500

    log:
      logSensitiveData: false
      queueLoggingInterval: 0
      FROST_LL: INFO
      FROST_LL_parser: INFO
      FROST_LL_queries: INFO
      FROST_LL_requests: INFO
      FROST_LL_service: DEBUG
      FROST_LL_settings: INFO
      FROST_LL_io_moquette: WARN
      FROST_LL_liquibase: INFO
      FROST_LL_org_jooq: INFO


  ###################################################
  # FROST-Server HTTP module configuration EXTERNAL #
  ###################################################
  httpExternal:
    # FROST-Server HTTP deployment settings
    urlSubPath:
    defaultCount: false
    defaultTop: 100
    maxTop: 1000
    maxDataSize: 25000000
    useAbsoluteNavigationLinks: true
    enableAuthentification: true
    mqttExposedEndpoints:
    persistence:
      countMode: "FULL" # SMAPLE_LIMIT | LIMIT_SAMPLE | LIMIT_ESTIMATE | ESTIMATE_LIMIT
      countEstimateThreshold: 10000
    extensions:
      customLinks:
        enable: true
        recursiveDepth: 0 # default: 0
      filterDelete:
        enable: false
    plugins:
      odata:
        enable: false
      openApi:
        enable: true

    resources:
      requests:
        cpu: 300m
        memory: 900Mi
      limits:
        cpu: 300m
        memory: 900Mi

    # Liveness probe configuration
    livenessProbe:
      enabled: false
      httpGet:
        path: /FROST-Server/v1.1/
        port: tomcat
      initialDelaySeconds: 30
      periodSeconds: 10
      timeoutSeconds: 1
      successThreshold: 1
      failureThreshold: 2

    readinessProbe:
      enabled: false
      httpGet:
        path: '/FROST-Server/v1.1/Things(6632)'
        port: tomcat
      initialDelaySeconds: 60
      periodSeconds: 10
      timeoutSeconds: 2
      successThreshold: 1
      failureThreshold: 2
    # Horizontal Pod Autoscaler
    hpa:
      enabled: true
      min: 1
      max: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    #-- either you set hpa.enabled: true or you use replicas
    replicas: 1

    ports:
      http:
        nodePort:
        servicePort: 80
    # if ingress is false, define a nodePort above
    ingress:
      enabled: true

    # FROST-Server HTTP business settings
    serviceHost: fqdn-frost.changeme.de
    serviceProtocol: https
    servicePort:

    # FROST-Server Authentication related settings
    auth:
      provider: de.fraunhofer.iosb.ilt.frostserver.auth.keycloak.KeycloakAuthProvider
      allowAnonymousRead: true
      roleRead: read
      roleCreate: create
      roleUpdate: update
      roleDelete: delete
      roleAdmin: admin
      keycloakConfigUrl: https://fqdn-keycloak.changeme.de/auth/realms/frostrealm/clients-registrations/install/frostserver
      keycloackConfigSecret: changeme-secret-from-frostserver-client-in-keycloak

    # FROST-Server Database related settings to the FROST-Server HTTP
    db:
      autoUpdate: true
      alwaysOrderbyId: false
      maximumConnection: 10
      maximumIdleConnection: 10
      minimumIdleConnection: 10
      queryTimeout: 0
      slowQueryThreshold: 200

      # if ip left blank, it'll take the name of the service
      ip: changeme-postgrsql-ip
      ports:
        postgresql:
          servicePort: 5432

      ## configuration for frost-server ##
      frostConnection:
        database: changeme-sensorthingsdb
        username: changeme-sensorthingsusr
        password: changeme

    # FROST-Server Messages Bus related settings to the FROST-Server HTTP
    bus:
      sendWorkerPoolSize: 100
      sendQueueSize: 1000
      recvWorkerPoolSize: 10
      recvQueueSize: 100
      maxInFlight: 500

    log:
      logSensitiveData: false
      queueLoggingInterval: 0
      FROST_LL: INFO
      FROST_LL_parser: INFO
      FROST_LL_queries: INFO
      FROST_LL_requests: INFO
      FROST_LL_service: DEBUG
      FROST_LL_settings: INFO
      FROST_LL_io_moquette: WARN
      FROST_LL_liquibase: INFO
      FROST_LL_org_jooq: INFO



  ###################################################
  # FROST-Server MQTT module configuration EXTERNAL #
  ###################################################
  mqtt:
    enabled: true
    image:
      registry: docker.io
      repository: fraunhoferiosb/frost-server-mqtt
      tag: 1.14.0
      pullPolicy: IfNotPresent
    # FROST-Server MQTT deployment settings
    replicas: 1
    hpa:
      enabled: false
      min: 1
      max: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    resources:
      enabled: false
      requests:
        cpu: 300m
        memory: 900Mi
      limits:
        cpu: 300m
        memory: 900Mi
    ports:
      mqtt:
        nodePort: 30883
        servicePort: 1883
      websocket:
        nodePort: 30876
        servicePort: 9876
    stickySessionTimeout: 10800 # timeout per client ip

    livenessProbe:
      enabled: false
      tcpSocket:
        port: 1883
      initialDelaySeconds: 30
      periodSeconds: 10
      timeoutSeconds: 60
      successThreshold: 1
      failureThreshold: 30

    readinessProbe:
      enabled: false
      tcpSocket:
        port: 1883
      initialDelaySeconds: 30
      periodSeconds: 10
      timeoutSeconds: 60
      failureThreshold: 30

    # FROST-Server MQTT business settings
    qos: 2
    subscribeMessageQueueSize: 1000
    subscribeThreadPoolSize: 20
    createMessageQueueSize: 100
    createThreadPoolSize: 10
    maxInFlight: 50
    waitForEnter: false
    sessionTimeoutSeconds: 3600  # default: 3600 seconds
    exposedEndpoints:
    messageSize: 8092 # default: 8092 bytes
    sessionQueueSize: 6144

    # FROST-Server Database related settings to the FROST-Server MQTT
    db:
      alwaysOrderbyId: false
      maximumConnection: 10
      maximumIdleConnection: 10
      minimumIdleConnection: 10
      queryTimeout: 0
      slowQueryThreshold: 200

          # if ip left blank, it'll take the name of the service
      ip: changeme-postgrsql-ip
      ports:
        postgresql:
          servicePort: 5432

      ## configuration for frost-server ##
      frostConnection:
        database: changeme-sensorthingsdb
        username: changeme-sensorthingsusr
        password: changeme

    # FROST-Server Messages Bus related settings to the FROST-Server MQTT
    bus:
      sendWorkerPoolSize: 10
      sendQueueSize: 100
      recvWorkerPoolSize: 10
      recvQueueSize: 100
      maxInFlight: 50

    log:
      logSensitiveData: false
      queueLoggingInterval: 0
      FROST_LL: INFO
      FROST_LL_parser: INFO
      FROST_LL_queries: INFO
      FROST_LL_requests: INFO
      FROST_LL_service: DEBUG
      FROST_LL_settings: INFO
      FROST_LL_io_moquette: WARN
      FROST_LL_liquibase: INFO
      FROST_LL_org_jooq: INFO


  ##################################################
  # FROST-Server Messages Bus module configuration #
  ##################################################
  bus:
    image:
        registry: docker.io
        repository: eclipse-mosquitto
        tag: 2.0.11
        pullPolicy: IfNotPresent
    # FROST-Server Messages Bus deployment settings
    ports:
      bus:
        servicePort: 1883

    # Common FROST-Server Messages Bus properties, shared by any other FROST-Server modules
    implementationClass: "de.fraunhofer.iosb.ilt.sta.messagebus.MqttMessageBus"
    topicName: "FROST-Bus"
    qos: 2
    config:
      mqtt: |-
        <max-queued-messages>1000</max-queued-messages>
    resources:
      requests:
        cpu:
        memory:
      limits:
        cpu:
        memory:


##################################################
# Nginx TLS termination module configuration     #
##################################################
nginxfrost:
  tcp:
    1883: "default/frost-hh-udp-iot-mqtt:1883"
  controller:
    config:
      # to use for keycloak redirect issues
      use-forwarded-headers: "true"
    nodeSelector:
      kubernetes.io/os: linux
    replicaCount: 2
    autoscaling:
      enabled: true
      minReplicas: 1
      maxReplicas: 1
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    # Define requests resources to avoid probe issues due to CPU utilization in busy nodes
    # ref: https://github.com/kubernetes/ingress-nginx/issues/4735#issuecomment-551204903
    # Ideally, there should be no limits.
    # https://engineering.indeedblog.com/blog/2019/12/cpu-throttling-regression-fix/
    resources:
    #  limits:
    #    cpu: 100m
    #    memory: 90Mi
    requests:
      cpu: 100m
      memory: 90Mi
    service:
    #  enableHttp: false
    #  enableHttps: false
      type: LoadBalancer
      loadBalancerIP: changeme-external-lb-frost-ip
    #metrics:
    #  enabled: true
    #headers: #--- relevant for ip logging in keycloak
    ingressClass: frostb
    admissionWebhooks:
      enabled: false
    ingressClassResource:
      enabled: true
  defaultBackend:
    enabled: true
    nodeSelector:
      kubernetes.io/os: linux
  ingressLables:
    rateLimits:
      enabled: true
      limitConnections: "10"
      limitRps: "10"
      limitRpm: "100"
      limitBurstMultiplier: "2"

nginxfrostinternal:
  enabled: false
  controller:
    metrics:
      enabled: false
      serviceMonitor:
          enabled: false
    nodeSelector:
      kubernetes.io/os: linux
    replicaCount: 2
    autoscaling:
      enabled: true
      minReplicas: 1
      maxReplicas: 2
      targetCPUUtilizationPercentage: 50
      targetMemoryUtilizationPercentage: 80
    # Define requests resources to avoid probe issues due to CPU utilization in busy nodes
    # ref: https://github.com/kubernetes/ingress-nginx/issues/4735#issuecomment-551204903
    # Ideally, there should be no limits.
    # https://engineering.indeedblog.com/blog/2019/12/cpu-throttling-regression-fix/
    resources:
    #  limits:
    #    cpu: 100m
    #    memory: 90Mi
    requests:
      cpu: 100m
      memory: 90Mi
    service:
      annotations:
        service.beta.kubernetes.io/azure-load-balancer-internal: "true"
    #  enableHttp: false
    #  enableHttps: false
        ## Set external traffic policy to: "Local" to preserve source IP on
    ## providers supporting it
    ## Ref: https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-typeloadbalancer
    # Get real IP's for rate limits per IP in nginx
      externalTrafficPolicy: "Local"
      type: LoadBalancer
      loadBalancerIP: 10.0.0.40
    ingressClassResource:
      name: frostb-internal
      enabled: true
      default: false
      controllerValue: "k8s.io/ingress-nginx"
    ingressClassByName: true
    # for backwards compatibility
    ingressClass: frostb
    admissionWebhooks:
      enabled: false
  defaultBackend:
    enabled: true
    nodeSelector:
      kubernetes.io/os: linux
  ingressLables:

cert:
  # ingress class annotation for all ingresses to bind to a specific ingress controller
  ingressClass: "frostb"
  # render the extra ca-cert template
  extra: true
  # enable templates ca-cert, ca-clusterissuer, route-ingress
  enabled: false
  issuerType: ClusterIssuer
  # sets acme to staging or prod
  productionEnabled: true
  #eMail: mike@contoso.de
  eMail: changeme@changeme.de
  frost:
    keySecretRef: secret-frost
    extraTlsHosts: |
      - fqdn-frost.changeme.de

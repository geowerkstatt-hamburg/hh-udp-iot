{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "frost-server.name" -}}
{{- default .Chart.Name .Values.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "frost-server.fullName" -}}
{{- $name := default .Chart.Name .Values.name -}}
{{- if .tier -}}
{{- printf "%s-%s-%s" .Release.Name $name .tier | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "frost-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Get the HTTP service API version
*/}}
{{- define "frost-server.httpGlobal.apiVersion" -}}
v1.0
{{- end -}}

{{/*
Get the HTTP service root URL: external
*/}}
{{- define "frost-server.httpExternal.serviceRootUrl" -}}
{{ .Values.frost.httpExternal.serviceProtocol }}://{{ .Values.frost.httpExternal.serviceHost }}{{ if .Values.frost.httpExternal.servicePort }}:{{ .Values.frost.httpExternal.servicePort }}{{ else if not .Values.frost.httpExternal.ingress.enabled }}:{{ .Values.frost.httpExternal.ports.http.nodePort }}{{ end }}{{ if .Values.frost.httpExternal.urlSubPath }}/{{ .Values.frost.httpExternal.urlSubPath }}{{ end }}
{{- end -}}

{{/*
Get the HTTP service root URL: internal
*/}}
{{- define "frost-server.httpInternal.serviceRootUrl" -}}
{{ .Values.frost.httpInternal.serviceProtocol }}://{{ .Values.frost.httpInternal.serviceHost }}{{ if .Values.frost.httpInternal.servicePort }}:{{ .Values.frost.httpInternal.servicePort }}{{ end }}{{ if .Values.frost.httpInternal.urlSubPath }}/{{ .Values.frost.httpInternal.urlSubPath }}{{ end }}
{{- end -}}

{{/*
{{/*
Create common labels.
*/}}
{{- define "frost-server.commonLabels" -}}
app.kubernetes.io/name: {{ include "frost-server.name" . }}
helm.sh/chart: {{ include "frost-server.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}
